package com.jiomitwa.akka.json.parsing

case class JsonParsingReq(inputFliePath:String, systemId:String,serialNo:String)
case class JsonFlattenFile(outPutFliePath:String)
case class BlobCityIngestionRes(message:String)