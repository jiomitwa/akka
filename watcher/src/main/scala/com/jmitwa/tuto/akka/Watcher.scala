package com.jmitwa.tuto.akka

import akka.actor.{ ActorSystem, Actor, ActorRef, Props }
import scala.collection.mutable.ListBuffer

object Watcher {

  def main(args: Array[String]): Unit = {

    implicit val system = ActorSystem("watcher-system")
    val idGenActor = system.actorOf(Props[IdGenActor], name = "idGenActor")
    var workareaDir = ""
    var inboundBaseDir = ""
    var qNames: String = ""
    var watcherActorsList = new ListBuffer[ActorRef]()

    for (arg <- args) {
      println(arg)
      val t = arg.split("=")

      (t(0), t(1)) match {
        case ("-in", inb) =>
          inboundBaseDir = inb
          println("-in---> " + inb)
        case ("-w", workarea) =>
          workareaDir = workarea
          println("workarea--->" + workarea)
        case ("-q", qArr) =>
          qNames = qArr
          println("-q--->" + qArr)
      }
    }

    val qArr = qNames.split(",")
    for (qName <- qArr) {
      val inboundDir = inboundBaseDir + "/" + qName
      println(inboundDir)
      val fileIODetails = new FileIODetails(inboundDir, workareaDir)
      val watcheractor1 = system.actorOf(Props(classOf[WatcherActor], idGenActor, fileIODetails), name = "watcheractor_" + qName)
      watcherActorsList += watcheractor1
    }

    val watcherActors = watcherActorsList.toArray
    for (a <- watcherActors) {
      a ! FileWatchJob
    }
  
    println("done")
    //system.stop(watcheractor)
    //system.terminate()
  }
}