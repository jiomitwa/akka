package com.jmitwa.tuto.akka

import java.io.File

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.actorRef2Scala

/**
 * @author sunil
 *
 */

case object FileWatchJob
case class FileIODetails(inbound: String, outbound: String)

class WatcherActor(idGenActor: ActorRef, fileIODetails: FileIODetails) extends Actor {
  def receive = {
    case FileWatchJob =>
      val fileArr = getFiles
    //  println("received job: " + fileArr.length)
      idGenActor ! NEW_ID_FILE_ARR(fileArr)

    case fileIds: FileWithIDArr =>
      for (fId <- fileIds.fileArray) {
        println(fId.file + " id: " + fId.id)
        FileUtil.moveFileToOutbound(fId.file, new File(fileIODetails.outbound + "/" + fId.id))
      }
      self ! FileWatchJob
    case _ => println("unknown request")
  }

  def getFiles: Array[File] = {
    val fileList: Array[File] = FileUtil.getAllFile(new File(fileIODetails.inbound))
    fileList
  }

}