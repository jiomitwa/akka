package com.jiomitwa.akka.json.parsing

import akka.actor.{Actor, Props, Terminated}
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}

class BolbCityIngRouter extends Actor{

  var router = {
    val routees = Vector.fill(5) {
      val r = context.actorOf(Props[BlobCityDataIngestionActor])
      context watch r
      ActorRefRoutee(r)
    }
    Router(RoundRobinRoutingLogic(), routees)
  }

  def receive = {
    case w: JsonFlattenFile ⇒
      router.route(w, sender())
    case Terminated(a) ⇒
      router = router.removeRoutee(a)
      val r = context.actorOf(Props[BlobCityDataIngestionActor])
      context watch r
      router = router.addRoutee(r)
  }
}