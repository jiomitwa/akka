package com.jiomitwa.akka.jbasic;

import java.util.HashMap;
import java.util.Map;

import com.jiomitwa.akka.jbasic.domain.CreateUser;
import com.jiomitwa.akka.jbasic.domain.User;

public class UserDBServeice {

	private Map<String, User> userMap = new HashMap<String, User>();

	public void createNewUser(CreateUser createUser) {
				userMap.put(createUser.user.userId, createUser.user);
		
		User newUser = createUser.user;
		
		System.out.printf("Creating new user ==>  %s %s %s \n",newUser.userId,newUser.name,newUser.mobileNo );
		
	}

	public User getUser(String userId) {
		return userMap.get(userId);
	}

}
