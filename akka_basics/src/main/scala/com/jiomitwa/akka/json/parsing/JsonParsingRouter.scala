package com.jiomitwa.akka.json.parsing

import akka.actor.Actor
import akka.routing.{ ActorRefRoutee, RoundRobinRoutingLogic, Router }
import akka.actor.Terminated
import akka.actor.Props

class JsonParsingRouter extends Actor{

  var router = {
    val routees = Vector.fill(5) {
      val r = context.actorOf(Props[JsonParsingActor])
      context watch r
      ActorRefRoutee(r)
    }
    Router(RoundRobinRoutingLogic(), routees)
  }

  def receive = {
    case w: JsonParsingReq ⇒
      router.route(w, sender())
    case Terminated(a) ⇒
      router = router.removeRoutee(a)
      val r = context.actorOf(Props[JsonParsingActor])
      context watch r
      router = router.addRoutee(r)
  }
}