package com.jiomitwa.akka.jbasic;

import java.util.ArrayList;
import java.util.List;

import com.jiomitwa.akka.jbasic.domain.CreateUser;
import com.jiomitwa.akka.jbasic.domain.GetUserDetails;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.routing.ActorRefRoutee;
import akka.routing.RoundRobinRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;

public class UserRouter extends AbstractActor {

	static public Props props()
	{
		return Props.create(UserRouter.class, () -> new UserRouter());
	}
	
	Router router;
	  {
	    List<Routee> routees = new ArrayList<Routee>();
	    for (int i = 0; i < 5; i++) {
	      ActorRef r = getContext().actorOf(Props.create(UserTestActor.class));
	      getContext().watch(r);
	      routees.add(new ActorRefRoutee(r));
	    }
	    router = new Router(new RoundRobinRoutingLogic(), routees);
	  }

	  @Override
	  public Receive createReceive() {
	    return receiveBuilder()
	      .match(CreateUser.class, message -> {
	        router.route(message, getSender());
	      }).match(GetUserDetails.class, message -> {
		        router.route(message, getSender());
		      })
	      .match(Terminated.class, message -> {
	        router = router.removeRoutee(message.actor());
	        ActorRef r = getContext().actorOf(Props.create(UserTestActor.class));
	        getContext().watch(r);
	        router = router.addRoutee(new ActorRefRoutee(r));
	      })
	      .build();
	  }

}
