package com.jiomitwa.akka.sample

import akka.actor.Actor

case object HelloMsg1
case object HelloMsg2
case object HelloMsg3
case object HelloMsg4
case class Welcome(name:String)

case class AskHelloResopnse(name: String)

class AskHelloActor extends Actor {

  def receive = {
    case HelloMsg1 => sender() ! "AskHelooActore sucessfull response : --> Hi "
    case HelloMsg2 => sender() ! "thanks" // do not send any response
    case HelloMsg3 => sender() ! AskHelloResopnse("Tunu-->Hi")
    case c:Welcome => println(c.name)
  }
}