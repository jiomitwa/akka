package com.jiomitwa.akka.sample

import akka.actor.Actor

case class HelloInMessage()
case object Hello
case object Hi

class HelloActor extends Actor{
  
  def receive = {
    case HelloInMessage => print("Hello")
    case Hello => for(i <- 1 to 10) println("hello object->"+i) 
                   sender() ! "Hi"
  }
}