package com.jiomitwa.akka.sample

import akka.actor.ActorSystem
import akka.actor.Props
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._


object Main {
  
  def main(args: Array[String]): Unit = {
        
    implicit val system = ActorSystem("akkabasicsmaple-system")
    
    val helloActor = system.actorOf(Props[HelloActor], "helloActor")
    val h = HelloInMessage
    
    helloActor ! Hello //fire and forget tell , asyncronious
    print("main thread")
   
    
  }
}