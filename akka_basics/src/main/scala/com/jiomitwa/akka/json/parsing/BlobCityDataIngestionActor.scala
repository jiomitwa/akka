package com.jiomitwa.akka.json.parsing
import akka.actor.Actor

class BlobCityDataIngestionActor extends Actor{

   def receive = {
    case jsonFlattenFile:JsonFlattenFile => sender ! ingestJsonPerfObject(jsonFlattenFile)
    case _ => println("Command not supported")
  }
  
  def ingestJsonPerfObject(jsonFlattenFile:JsonFlattenFile):BlobCityIngestionRes = {
    println(s"${jsonFlattenFile.outPutFliePath}")
    BlobCityIngestionRes("Ingestion completed")
  }
}