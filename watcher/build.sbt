name := "watcher"

version := "1.0"

scalaVersion := "2.11.7"
val akkaV       = "2.5.2"


EclipseKeys.withSource := true

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaV,
  "commons-io" % "commons-io" % "2.5",
  "junit" % "junit" % "4.12"
 )