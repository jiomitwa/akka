package com.jiomitwa.akka.sample.user

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.util.{Failure, Success}
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Success

import akka.actor.ActorSystem
import akka.actor.Props
import akka.pattern.ask
import akka.util.Timeout

class UserMain {




  def main(args: Array[String]): Unit = {
    implicit val timeout = Timeout(5 seconds)
    implicit val system = ActorSystem("akkabasicsmaple-system")
    implicit val ec = system.dispatcher

    val userActor = system.actorOf(Props[UserActor], "userActor")

    val userFutureActor = userActor ? UserId("CONS00130211110011234")
    val f1: Future[Option[UserProfile]] = userFutureActor.mapTo[Option[UserProfile]]

    f1 onComplete {
      case Success(result) => {
         println("Future onComplete Success :=>" + result)
         result match {
         case Some(userProfile:UserProfile) => println(userProfile)
         case None => println("nonn")
         }
      }
                             
      case Failure(failure) => println("DB is down=>" + failure)
    }
  }
}