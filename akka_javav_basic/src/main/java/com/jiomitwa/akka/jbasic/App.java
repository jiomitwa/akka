package com.jiomitwa.akka.jbasic;

import com.jiomitwa.akka.jbasic.domain.CreateUser;
import com.jiomitwa.akka.jbasic.domain.GetUserDetails;
import com.jiomitwa.akka.jbasic.domain.User;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.pattern.Patterns;
import akka.util.Timeout;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ActorSystem actorSystem = ActorSystem.create("UserActorSystem");
        
        ActorRef actorRef = actorSystem.actorOf(UserTestActor.props(),"userTestActor");
        
        User user = new User("1","Sunil","222234");
        CreateUser createUser = new CreateUser(user);
        actorRef.tell(createUser, ActorRef.noSender());
        
     // send the message to getUserDetails
    	GetUserDetails getUserDetails = new GetUserDetails();
    	getUserDetails.userId = "1";
    	
    	Timeout timeout = new Timeout(Duration.create(5, "seconds"));
    	Future<Object> future = Patterns.ask(actorRef, getUserDetails, timeout);
    	try
    	{
    	User user2 = (User) Await.result(future, timeout.duration());
    	System.out.println("Received data"+user2.userId + " " + user2.mobileNo + " " + user2.name);
    	} catch (Exception e)
    	{
    	// TODO Auto-generated catch block
    	e.printStackTrace();
    	}
        
    }
}
