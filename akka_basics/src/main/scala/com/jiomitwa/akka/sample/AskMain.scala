package com.jiomitwa.akka.sample

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Success

import akka.actor.ActorSystem
import akka.actor.Props
import akka.pattern.ask
import akka.util.Timeout

object AskMain {
  implicit val timeout = Timeout(5 seconds)
  def main(args: Array[String]): Unit = {

    implicit val system = ActorSystem("akkabasicsmaple-system")
    implicit val ec = system.dispatcher
    val askHelloActor = system.actorOf(Props[AskHelloActor], "askHelloActor")


    
    val askHelloFuture = askHelloActor ? HelloMsg1
    val f1: Future[String] = askHelloFuture.mapTo[String]

    f1 onComplete {    
      case Success(result) => println("Future onComplete Success :=>" + result)
      case Failure(failure) => println("Future onComplete Failure:=>" + failure)
    }
    
     println("I am in main sreen test1")
        
    askHelloActor ? Welcome("Sunil")
    askHelloActor ? Welcome("Jijo")
    askHelloActor ? Welcome("Tunu")
    
    println("I am in main creen")
    

    val askHelloFuture2 = askHelloActor ? HelloMsg2
    val f2: Future[String] = askHelloFuture2.mapTo[String]

    f2 onComplete {
      case Success(result) => println("Future onComplete Success :=>" + result)
      case Failure(failure) => println("Future onComplete Failure:=>" + failure)
    }

    val askHelloFuture3 = askHelloActor ? HelloMsg3
    val f3: Future[AskHelloResopnse] = askHelloFuture3.mapTo[AskHelloResopnse]
    f3 onComplete { result => println("in sucess case" + result.get.name) //here result is Try[AskHelloResopnse]
      /* case Success(result)  => println("in sucess case" + result)
       case Failure(failure) => println(failure)*/
    }
    
    val askHelloFuture3_1 = askHelloActor ? HelloMsg3
    val f3_1: Future[AskHelloResopnse] = askHelloFuture3_1.mapTo[AskHelloResopnse]
    f3 onComplete { //result => println("askHelloFuture3_1 in sucess case" + result.get.name) //here result is Try[AskHelloResopnse]
      case Success(result)  => println("askHelloFuture3_1 in sucess case" + result.name)
       case Failure(failure) => println(failure)
    }
    
    val askHelloFuture4 = askHelloActor ? HelloMsg2
    val f4: Future[AskHelloResopnse] = askHelloFuture4.mapTo[AskHelloResopnse]
    f4 onComplete { result => println("in sucess case" + result.get.name) //here result is Try[AskHelloResopnse]
      /* case Success(result)  => println("in sucess case" + result)
       case Failure(failure) => println(failure)*/
    }
  }
}