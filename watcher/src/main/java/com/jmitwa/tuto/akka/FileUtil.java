package com.jmitwa.tuto.akka;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class FileUtil {

	public static File[] getAllFile(File inputFolder) {
		File[] listOfFiles = inputFolder.listFiles();

		for (File file : listOfFiles) {
			if (file.isFile()) {
				System.out.println(file.getName());
			}
		}
		return listOfFiles;
	}

	public static void moveFileToOutbound(File srcFile, File destDir) {
		try {
			FileUtils.moveFileToDirectory(srcFile, destDir, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
