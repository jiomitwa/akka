package com.jiomitwa.akka.jbasic;

import com.jiomitwa.akka.jbasic.domain.CreateUser;
import com.jiomitwa.akka.jbasic.domain.GetUserDetails;

import akka.actor.AbstractActor;
import akka.actor.Props;

public class UserTestActor extends AbstractActor {

	UserDBServeice userDBServeice = new UserDBServeice();
	
	static public Props props()
	{
		return Props.create(UserTestActor.class, () -> new UserTestActor());
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
			      .match(CreateUser.class, createUser -> {
			    	  System.out.println("self"+getSelf());
			    	  userDBServeice.createNewUser(createUser);
			      }).match(GetUserDetails.class, getUserDetails -> {
			    	  System.out.println("sender" + getSender());
			    	  System.out.println("self"+getSelf());
			    	  getSender().tell(userDBServeice.getUser(getUserDetails.userId),getSelf());
			      })
			      .matchAny(o -> System.out.println("received unknown message"))
			      .build();
			  }
	

	
}
