package com.jiomitwa.akka.jbasic.domain;

public class User {
	public String userId;
	public String name;
	public String mobileNo;
	
	public User(String userId, String name, String mobileNo) {
			this.name = name;
			this.userId = userId;
			this.mobileNo = mobileNo;
			}
	public User()
	{}
}
