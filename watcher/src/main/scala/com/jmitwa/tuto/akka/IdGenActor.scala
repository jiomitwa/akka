package com.jmitwa.tuto.akka

import java.io.File
import java.text.SimpleDateFormat
import java.util.Calendar

import scala.collection.mutable.ListBuffer

import akka.actor.Actor
import akka.pattern.ask

case class FileWithID(file:File,id:String)
case class NEW_ID_FILE_ARR(fileArray:Array[File])
case class FileWithIDArr(fileArray:Array[FileWithID])
case object NEW_ID
case class ID(id:String)

class IdGenActor extends Actor{
  var currentCount:Int =0
  var previousDate = ""
  
  def receive = {
    case NEW_ID => 
                 val id = getId
                 sender ! ID(id)
                 
    case files:NEW_ID_FILE_ARR => 
                  var fileList = new ListBuffer[FileWithID]()
                  for(file<-files.fileArray)
                   {
                     val newId = getId
                     val fileWithID = new FileWithID(file,newId)
                     fileList+=fileWithID
                   }
                  val fileArr = fileList.toArray 
                  sender ! FileWithIDArr(fileArr)
                  
    case _ => println("unknown request")
  }
  
  def getId : String = {
    val cal = Calendar.getInstance()
    val dFormat = new SimpleDateFormat("yyyyMMddHHmmss")
    val currentDate = dFormat.format(cal.getTime)
    if(!currentDate.equalsIgnoreCase(previousDate))
    { 
      previousDate = currentDate
      currentCount=0
    }
     currentCount= currentCount +1
     val num = "%04d" format currentCount
     currentDate+num
  }
  
}