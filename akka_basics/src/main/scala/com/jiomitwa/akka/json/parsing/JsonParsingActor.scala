package com.jiomitwa.akka.json.parsing

import akka.actor.Actor


class JsonParsingActor extends Actor {
  
  def receive = {
    case jsonParsingReq:JsonParsingReq => sender ! flattenObjectJsonFile(jsonParsingReq)
    case _ => println("Command not supported")
  }
  
  def flattenObjectJsonFile(jsonParsingReq:JsonParsingReq):JsonFlattenFile = {
    println(s"${jsonParsingReq.inputFliePath} ${jsonParsingReq.systemId} ${jsonParsingReq.serialNo}")
    val outputFilePath="c:/output"
    JsonFlattenFile(outputFilePath)
  }
}