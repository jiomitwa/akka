package com.jiomitwa.akka.sample.user

import akka.actor.Actor

class UserActor extends Actor{
  
  def receive = {
    case userId:UserId => println("fatching profile for userId "+ userId.userID)
                          //fetch from DB
                          // rest call
                          sender() ! Option(UserProfile(userId.userID,"Tunu"))
  }
  
}