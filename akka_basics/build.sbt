name := "akka_basic"

version := "1.0"

scalaVersion := "2.11.8"

EclipseKeys.withSource := true

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

val akkaHttpV = "10.0.10"
val scalaTestV = "3.0.4"
val akkaV    = "2.5.4"
libraryDependencies ++= Seq(
	
  "com.typesafe.akka" %% "akka-http" % "10.0.10",
  "com.typesafe.akka" %% "akka-actor" % "2.5.4",
  "com.typesafe.akka" %% "akka-stream" % "2.5.4",  
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.10",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.1.0-RC1",
  "org.scalatest" %% "scalatest" % scalaTestV % "test",
  "org.mongodb" % "mongodb-driver" % "3.6.3",
  "com.google.code.gson" % "gson" % "2.8.2",
  "com.typesafe" % "config" % "1.3.3",
  // logging
  "com.typesafe.akka" %% "akka-slf4j" % akkaV,
  "ch.qos.logback" % "logback-classic" % "1.2.3"
)