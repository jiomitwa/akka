package com.jiomitwa.akka.json.parsing

import akka.actor.{ActorSystem, Props}
import akka.util.Timeout
import com.jiomitwa.akka.sample.AskHelloActor

import scala.concurrent.Future
import scala.util.{Failure, Success}
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Success

import akka.actor.ActorSystem
import akka.actor.Props
import akka.pattern.ask
import akka.util.Timeout

object BlobCityMain {

  implicit val timeout = Timeout(5 seconds)
  def main(args: Array[String]): Unit = {

    implicit val system = ActorSystem("akkabasicsmaple-system")
    implicit val ec = system.dispatcher
    val jsonParsingRouter = system.actorOf(Props[JsonParsingRouter], "askHelloActor")
    val blobCityDataIngestionRoute = system.actorOf(Props[BlobCityDataIngestionActor], "blobCityDataIngestionActor")

    val jsonParserFuture = jsonParsingRouter ? JsonParsingReq("c:/input","sys","serial")
    val f1: Future[JsonFlattenFile] = jsonParserFuture.mapTo[JsonFlattenFile]

    f1 onComplete {
      case Success(result) => {
        println("Future onComplete Success :=>" + result)
        val ingesFuture = blobCityDataIngestionRoute ? result
        val blobFuture: Future[BlobCityIngestionRes] = ingesFuture.mapTo[BlobCityIngestionRes]
        blobFuture onComplete {
          case Success(result) => println("Future onComplete Success :=>" + result)
          case Failure(failure) => println("Future onComplete Failure:=>" + failure)
        }
      }

      case Failure(failure) => println("Future onComplete Failure:=>" + failure)
    }
  }
  }
