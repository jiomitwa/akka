Software requirement
1. Java 8
2. eclipse latest version
3. eclipse plug-in for scala

Build the watcher using maven
1. mvn clean package
2. mvn eclipse:eclipse
3. import project in eclipse
4. if src/main/scala is not visisble in eclipse package add folder in java build sorce path 


Running watcher service
1. create inbound folder
  a. C:/netapp_work/bitbucket_repo_my/jiomitwa/akka/watcher/test_data/inbound
  b. create queue inside inbound
    	PUT,POST,SMTP
2. create workarea folder
   C:/netapp_work/bitbucket_repo_my/jiomitwa/akka/watcher/test_data/workarea

Running from ecllipse
1. main class   Watcher
2. Pass following argument in runtime
   -in="C:/netapp_work/bitbucket_repo_my/jiomitwa/akka/watcher/test_data/inbound" -w="C:/netapp_work/bitbucket_repo_my/jiomitwa/akka/watcher/test_data/workarea" -q="PUT,SMTP,POST"
   
   
Running from console
use below command
java -cp watcher-0.0.1-SNAPSHOT-allinone.jar com.jmitwa.tuto.akka.Watcher -in="C:/netapp_work/bitbucket_repo_my/jiomitwa/akka/watcher/test_data/inbound" -w="C:/netapp_work/bitbucket_repo_my/jiomitwa/akka/watcher/test_data/workarea" -q="PUT,SMTP,POST"

Building Docker images
1. crate a new seprate directry in C:\jiomitwa\docker_image\watcher\
2. copy watcher-0.0.1-SNAPSHOT-allinone.jar to C:\jiomitwa\docker_image\watcher\
3. copy test_data from watcher to C:\jiomitwa\docker_image\watcher\
4. copy Dokerfile to C:\jiomitwa\docker_image\watcher\
5. CD C:\jiomitwa\docker_image\watcher\
6. build docker image: docker build -t watcher . 
7. run docker image :  docker run -it watcher /bin/bash
8. inside docker run watcher program with below command
java -cp watcher-0.0.1-SNAPSHOT-allinone.jar com.jmitwa.tuto.akka.Watcher -in="/app/test_data/inbound" -w="/app/test_data/workarea" -q="PUT,SMTP,POST"
9. copy some sampel test data from inbound_backup to inbound : cp -r /app/test_data/inbound_backup/PUT /app/test_data/inbound/PUT
10. check new id got generated in /app/testdata/workares
